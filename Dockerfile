# install latest node
# https://service.docker.com/_/node/
FROM node:lts-slim

ENV PORT=8080

# create and set app directory
RUN mkdir -p /usr/app/
WORKDIR /usr/app/

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
COPY package*.json ./

# Bundle app source
COPY . .

RUN npm install
# If you are building your code for production
# RUN npm install --only=production
RUN npm run build

# expose container port
EXPOSE ${PORT}

CMD ["npm", "run", "start"]
