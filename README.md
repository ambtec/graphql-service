# Database Service

## How to start

A.) Install Docker first.

B.) After open your console and navigate to your repository folder. \
1a.) Run `` docker build --no-cache -t database-service . `` to build the container \
2a.) Then run `` docker run -it -p 4200:8080 --env-file=.env.dev database-service `` to start the container

C.) Open your browser and navigate to the address shown in the console output, by default it is `` http://127.0.0.1:4200 `` \
![GraphQL Example](./assets/graphql-example.png "GraphQL Example")

D.) To stop a running docker instance use your console. \
3a.) Run `` docker ps `` to get a list of all running docker instances. \
Find `` node-red-service `` and copy the container id. \
4a.) Run `` docker kill ${CONTAINER_ID} `` to stop the container.

E.) If your machine use a different port for docker open console. \
Run `` docker ps `` to get a list of all running docker instances. \
Find `` node-red-service `` and copy the container id. \
Run `` docker container inspect ${CONTAINER_ID} | grep -i IpAddress `` to fetch the current ip address.

### Params
`` PORT ``: 4200

Environment \
`` PRODUCTION ``: true|false \
`` DATABASE_HOST ``: string \
`` DATABASE_PORT ``: number (optional) \
`` DATABASE_USER ``: string \
`` DATABASE_PASSWORD ``: string \
`` DATABASE_PROJECT ``: string \
`` DATABASE_DIALECT ``: mysql | mariadb

## Service setup

The service is realized with Sequelize and GraphQL.

### Sequelize

All tables are maintenanced in `` Models/ ``. In there your will find all table definition know to the service. \
Every changes in models must match the database tables. It need to be applied by hand to it or you can use the Sequelize `` sync `` option to apply the models automatically on container startup:

```
models.sequelize.sync({
  force: true
});
```

Attention: `` Sync does delete content while updating the database stucture. Therefor handle with care! ``

### GraphQL

GraphQL consume the models and provide all queries based on them which you can find in `` resolvers/ ``. The example shows how a CRUD setup can be achieved.


A collection of demo queries.

#### Role


##### Get all

```sh
query {
  roles {
    id,
    name,
    description,
    users {
      id,
      username
    }
  }
}
```

##### Get by Id, Name

```sh
query {
  role(id: "<ID>", name: "<NAME>") {
    id,
    name,
    description,
    users {
      id,
      username
    }
  }
}
```

##### Create new

```sh
mutation {
  createRole(input: {
    name: "ALL",
    description: "Role description"
  }) {
    id
  }
}
```

##### Update by Id

```sh
mutation {
  updateRole(id: "<ID>", input: {
    name: "ALL",
    description: "Another role description"
  }) {
    id
  }
}
```

##### Delete by Id

```sh
mutation {
  removeRole(id: "<ID>") {
    id
  }
}
```

#### User

##### Get all

```sh
query {
  users {
    id,
    username,
    email,
    roles {
      id,
      name,
      description
    }
  }
}
```

##### Get by Id, Email

```sh
query {
  user(id: "<ID>", email: "<EMAIL>") {
    id,
    username,
    email,
    roles {
      id,
      name,
      description
    }
  }
}
```

##### Create new

```sh
mutation {
  createUser(input: {
    username: "Lorem Ipsum",
    email: "hello@world.ch",
    roles: [
      { roleId: "<ID>" },
      { roleId: "<ID>" },
      { roleId: "<ID>" }
    ]
  }) {
    id
  }
}
```

##### Update by Id

```sh
mutation {
  updateUser(id: "<ID>", input: {
    username: "Lorem ipsum dolores ips",
    email: "hello@world.ch",
    roles: [
      { roleId: "<ID>" },
      { roleId: "<ID>" }
    ]
  }) {
    id
  }
}
```

##### Delete by Id

```sh
mutation {
  removeUser(id: "<ID>") {
    id
  }
}
```

#### UserRole

##### Get all

```sh
query {
  userroles {
    id,
    userId,
    roleId
  }
}
```

##### Get by Id, UserId, RoleId

```sh
query {
  userrole(id: "<ID>", userId: "<ID>", roleId: "<ID>") {
    id,
    userId,
    roleId
  }
}
```

##### Delete by Id

```sh
mutation {
  removeUserRole(id: "<ID>") {
    id
  }
}
```

## License
MIT