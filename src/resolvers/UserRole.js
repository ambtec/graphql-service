import { resolver } from 'graphql-sequelize';
import models from '../db';

// UserRole.js
export const typeDef = `
type UserRole {
  id: ID!
  userId: ID!
  roleId: ID!
}

input UserRoleInput {
  userId: ID
  roleId: ID
}

extend type Mutation {
  createUserRole(input: UserRoleInput): UserRole
  removeUserRole(id: ID!): UserRole
}

extend type Query {
  userrole(id: ID!): UserRole
  userroles(userId: ID, roleId: ID): [UserRole]
}
`;

export const resolvers = {
  Query: {
    userrole: resolver(models.UserRole),
    userroles: resolver(models.UserRole),
  },
  UserRole: {
    // roles: resolver(models.User.Roles),
  },
  Mutation: {
    createUserRole: function (root, { input }) {
      return models.UserRole.create(input);
    },
    removeUserRole: function (root, { id }) {
      return models.UserRole.count({ 'where': { 'id': id } })
        .then(count => {
          if (count > 0) {
            models.UserRole.destroy({ 'where': { 'id': id } });
            return { 'id': id };
          }
          return { 'id': null };
        });
    },
  }
};
