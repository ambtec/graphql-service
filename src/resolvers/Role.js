import { resolver } from 'graphql-sequelize';
import models from '../db';

// Role.js
export const typeDef = `
type Role {
  id: ID!
  name: String
  description: String
  users: [User]
}

input RoleInput {
  id: Int
  name: String
  description: String
}

extend type Mutation {
  createRole(input: RoleInput): Role
  updateRole(id: ID!, input: RoleInput): Role
  removeRole(id: ID!): Role
}

extend type Query {
  role(id: ID!): Role
  roles(name: String): [Role]
}
`;

export const resolvers = {
  Query: {
    role: resolver(models.Role),
    roles: resolver(models.Role),
  },
  Role: {
    users: resolver(models.Role.Users),
  },
  Mutation: {
    createRole: function (root, { input }) {
      return models.Role.create(input);
    },
    updateRole: function (root, { id, input }) {
      return models.Role.count({ 'where': { 'id': id } })
        .then(count => {
          if (count > 0) {
            models.Role.update(input, { 'where': { 'id': id } });
            return { 'id': id };
          }
          return { 'id': null };
        });
    },
    removeRole: function (root, { id }) {
      return models.Role.count({ 'where': { 'id': id } })
        .then(count => {
          if (count > 0) {
            models.Role.destroy({ 'where': { 'id': id } });
            models.UserRole.destroy({ 'where': { 'roleId': id } });
            return { 'id': id };
          }
          return { 'id': null };
        });
    },
  }
};
