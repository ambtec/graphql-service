import { resolver } from 'graphql-sequelize';
import models from '../db';

// User.js
export const typeDef = `
type User {
  id: ID!
  username: String!
  email: String!
  roles: [Role]
}

input UserInput {
  username: String
  email: String
  roles: [UserRoleInput]
}

extend type Mutation {
  createUser(input: UserInput): User
  updateUser(id: ID!, input: UserInput): User
  removeUser(id: ID!): User
}

extend type Query {
  user(id: ID!): User
  users(email: String): [User]
}
`;

export const resolvers = {
  Query: {
    user: resolver(models.User),
    users: resolver(models.User),
  },
  User: {
    roles: resolver(models.User.Roles),
  },
  Mutation: {
    createUser: function (root, { input }) {
      return models.User.create(input)
        .then(user => {
          if (input.roles) {
            for (let i = 0; i < input.roles.length; i++) {
              models.UserRole.create({ 'userId': user.dataValues.id, 'roleId': input.roles[i].roleId });
            }
          }
          return { 'id': user.dataValues.id };
        });
    },
    updateUser: function (root, { id, input }) {
      return models.User.count({ 'where': { 'id': id } })
        .then(count => {
          if (count > 0) {
            models.User.update(input, { 'where': { 'id': id } })
              .then(user => {
                if (input.roles) {
                  models.UserRole.destroy({ 'where': { 'userId': id } });
                  for (let i = 0; i < input.roles.length; i++) {
                    models.UserRole.create({ 'userId': id, 'roleId': input.roles[i].roleId });
                  }
                }
              });
            return { 'id': id };
          }
          return { 'id': null };
        });
    },
    removeUser: function (root, { id }) {
      return models.User.count({ 'where': { 'id': id } })
        .then(count => {
          if (count > 0) {
            models.User.destroy({ 'where': { 'id': id } });
            models.UserRole.destroy({ 'where': { 'userId': id } });
            return { 'id': id };
          }
          return { 'id': null };
        });
    },
  }
};
