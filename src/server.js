import { GraphQLServer } from 'graphql-yoga';
import { createContext, EXPECTED_OPTIONS_KEY } from 'dataloader-sequelize';
import { resolver } from 'graphql-sequelize';
import depthLimit from 'graphql-depth-limit'
import { merge } from 'lodash';

// Load DB models
import models from './db';

// Load schema and resolvers [start]
import { typeDef as UserDef, resolvers as UserRes } from './resolvers/User.js';
import { typeDef as RoleDef, resolvers as RoleRes } from './resolvers/Role.js';
import { typeDef as UserRoleDef, resolvers as UserRoleRes } from './resolvers/UserRole.js';
// Load schema and resolvers [end]

const Query = `
type Query {
  _empty: String
}

type Mutation {
  _empty: String
}
`;

const resolvers = {};

// Tell `graphql-sequelize` where to find the DataLoader context in the
// global request context
resolver.contextToOptions = {
  [EXPECTED_OPTIONS_KEY]: EXPECTED_OPTIONS_KEY
};

function init() {
  // Sync DB on startup
  if (process.env.DATABASE_SYNC === true) {
    // Run sync cmd
    models.sequelize.sync({
      force: true
    });
  }

  const server = new GraphQLServer({
    typeDefs: [
      Query,
      UserDef,
      RoleDef,
      UserRoleDef
    ],
    resolvers: merge(
      resolvers,
      UserRes,
      RoleRes,
      UserRoleRes
    ),
    validationRules: [depthLimit(10, {
      ignore: [/_trusted$/, 'idontcare']
    },
      depths => console.log(depths))],
    context(req) {
      // For each request, create a DataLoader context for Sequelize to use
      const dataloaderContext = createContext(models.sequelize);

      // Using the same EXPECTED_OPTIONS_KEY, store the DataLoader context
      // in the global request context
      return {
        [EXPECTED_OPTIONS_KEY]: dataloaderContext,
      };
    },
  });

  return server;
}

export default init();
