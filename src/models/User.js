import DataTypes, { Model } from 'sequelize';

class User extends Model {
  static tableName = 'users';

  static schema = {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false,
      primaryKey: true
    },
    username: {
      type: DataTypes.STRING(255)
    },
    email: {
      type: DataTypes.STRING(255),
      unique: true,
      validate: {
        isEmail: true
      }
    },
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.literal("NOW()"),
      allowNull: false
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.literal("NOW()"),
      allowNull: false
    }
  };

  static associate(models) {
    User.Roles = User.belongsToMany(models.Role, {
      through: {
        model: models.UserRole,
        unique: false
      },
      foreignKey: 'userId',
      onDelete: 'CASCADE'
    });
  }
}

export default (sequelize) => {
  User.init(User.schema, {
    sequelize,
    tableName: User.tableName,
  });

  return User;
};
