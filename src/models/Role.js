import DataTypes, { Model } from 'sequelize';

class Role extends Model {
  static tableName = 'roles';

  static schema = {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: DataTypes.ENUM('ER', 'ALL', 'DL')
    },
    description: {
      type: DataTypes.TEXT
    },
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.literal("NOW()"),
      allowNull: false
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.literal("NOW()"),
      allowNull: false
    }
  };

  static associate(models) {
    Role.Users = Role.belongsToMany(models.User, {
      through: {
        model: models.UserRole,
        unique: false
      },
      foreignKey: 'roleId',
      onDelete: 'CASCADE'
    });
  }
}

export default (sequelize) => {
  Role.init(Role.schema, {
    sequelize,
    tableName: Role.tableName,
  });

  return Role;
};
