import DataTypes, { Model } from 'sequelize';

class UserRole extends Model {
  static tableName = 'user_role';

  static schema = {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false,
      primaryKey: true
    },
    userId: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false
    },
    roleId: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false
    },
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.literal("NOW()"),
      allowNull: false
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.literal("NOW()"),
      allowNull: false
    }
  };

  static associate(models) {
  }
}

export default (sequelize) => {
  UserRole.init(UserRole.schema, {
    sequelize,
    tableName: UserRole.tableName,
  });

  return UserRole;
};
