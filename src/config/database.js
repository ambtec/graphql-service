import Sequelize from 'sequelize';

const sequelizeConf = {
  host: process.env.DATABASE_HOST,
  dialect: process.env.DATABASE_DIALECT
};

if (process.env.DATABASE_PORT) {
  sequelizeConf['port'] = process.env.DATABASE_PORT;
}

// https://sequelize.readthedocs.io/en/v3/docs/getting-started/
const sequelize = new Sequelize(process.env.DATABASE_PROJECT, process.env.DATABASE_USER, process.env.DATABASE_PASSWORD, sequelizeConf);

export default sequelize;
