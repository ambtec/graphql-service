import server from './server';

const options = {
  port: process.env.PORT,
  endpoint: '/',
  subscriptions: '/',
  playground: '/',
}

// Start the GraphQL server
server.start(options, ({
  port
}) => {
  console.log(`Server started, listening on port ${port} for incoming requests.`);
});
